<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link href="assets/css/bootstrap.min-3.3.6.css" rel="stylesheet">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel='stylesheet'
	href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'>

<script src="assetsjs/jquery-1.12.1.min.js"></script>
<script src="assets/js/bootstrap.min-3.3.6.js"></script>
</head>
<body>
	<div class="container">
		<h1>Edit Profile</h1>
		<hr>
		<div class="row">
			<!-- left column -->
			<div class="col-md-3">
				<!-- <div class="text-center">
          <img src="//placehold.it/100" class="avatar img-circle" alt="avatar">
          <h6>Upload a different photo...</h6>
          
          <input type="file" class="form-control">
        </div> -->
			</div>

			<!-- edit form column -->
			<div class="col-md-6 personal-info">
				<!-- <div class="alert alert-info alert-dismissable">
					<a class="panel-close close" data-dismiss="alert">�</a> <i
						class="fa fa-coffee"></i> This is an <strong>.alert</strong>. Use
					this to show important messages to the user.
				</div> -->
				<h3>Personal info</h3>

				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label class="col-lg-3 control-label">First name:</label>
						<div class="col-lg-8">
							<input class="form-control" type="text" value="Jane">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Middle name:</label>
						<div class="col-lg-8">
							<input class="form-control" type="text" value="Bishop">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">last Name:</label>
						<div class="col-lg-8">
							<input class="form-control" type="text" value="">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Email:</label>
						<div class="col-lg-8">
							<input class="form-control" type="text" value="abc@gmail.com">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Password:</label>
						<div class="col-md-8">
							<input class="form-control" type="password" value="">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Confirm password:</label>
						<div class="col-md-8">
							<input class="form-control" type="password" value="">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">District Name:</label>
						<div class="col-lg-8">
							<input class="form-control" type="text" value="Jane">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Block Name:</label>
						<div class="col-lg-8">
							<input class="form-control" type="text" value="Jane">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">panchayat Name:</label>
						<div class="col-lg-8">
							<input class="form-control" type="text" value="Jane">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label"></label>
						<div class="col-md-8">
							<input type="button" class="btn btn-primary" value="Save Changes">
							<span></span> <input type="reset" class="btn btn-default"
								value="Cancel">
						</div>
					</div>
				</form>
			</div>
			<div class="col-md-3"></div>
		</div>
	</div>
</body>
</html>
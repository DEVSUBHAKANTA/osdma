<!DOCTYPE html>
<html lang="en">
<head>
<title>Bootstrap Example</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/login.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<script src="/assets/coustomjs/loginValidation.js"></script>
<script>
	function myFunction() {
		var x = document.getElementById("pwd");
		if (x.type === "password") {
			x.type = "text";
		} else {
			x.type = "password";
		}
	}
</script>

</head>

<header>
	
</header>
<body>
	<hr>

	<div class="container-fluid">
		<div class="row content">
			<div class="col-sm-6 sidenav">

				<h3 style="margin-left: 70px; color: blue">
					<b>User Credential</b>
				</h3>

				<!--       <h4>John's Blog</h4> -->
				<form class="form-horizontal" action="/signup" method="post">
					<div class="form-group">
						<label class="control-label col-sm-3" for="user">UserId:</label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="user" id="username" 
								placeholder="Enter User-Id" autocomplete="off"> 								
								<span
								style="position: absolute; right: 20px; top: 8px; color: green;"
								class="fa fa-user "></span>								
								<span class="errorMsg" id="errorusername">*Please provide the valid username.</span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-3" for="pwd">Password:</label>
						<div class="col-sm-5">
							<input type="password" class="form-control" id="userpassword" name="password"
								placeholder="Enter password" autocomplete="off"> 
								
								<span style="position: absolute; right: 8px; top: 10px; color: green;"
								class="fa fa-lock form-control-feedback"></span>
								<span class="errorMsg" id="errorpassword" >*Please provide the valid password.</span>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-9">
							<input type="submit" class="btn btn-primary" id="submitform" value="submit">
							<span style="margin-left: 70px;">
							
							<input type="checkbox" onclick="myFunction()">Show Password</span>
						</div>
					</div>
				</form>
				<p align="justify">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Orissa State Disaster Mitigation
					Authority (OSDMA) was set up by the Government of Orissa as an
					autonomous organization vide Finance Department Resolution No. IFC-
					74/99-51779/F dated the 28<sup>th</sup> December 1999 ( in the
					intermediate aftermath of the Super-cyclone in 1999). It was
					registered under the Societies Registration Act, 1860 on 29.12.1999
					as a non-profit making &amp; charitable institution for the
					interest of the people of Orissa, with its headquarters at
					Bhubaneswar and jurisdiction over the whole State. The Department
					of Revenue is the administrative department of OSDMA vide Revenue
					Department Resolution No.39373/R dated 26<sup>th</sup> August 2000.
					Subsequently, the name of the Authority was changed from Orissa
					State Disaster Mitigation Authority to <font color="#0000ff"><strong>Orissa
							State Disaster Management Authority</strong></font> vide Revenue &amp; Disaster
					Management Department Resolution No. 42317/R&amp;DM dated 27th
					September, 2008.&nbsp;
				</p>
			</div>
			<div class="col-sm-6">
				<div>
					<img src="../assets/images/orm.jpg" alt="map">
				</div>
			</div>
		</div>
	</div>


	<!-- 	<footer class="container-fluid text-center"> -->
	<!-- 		<p>Footer Text</p> -->
	<!-- 	</footer> -->


</body>
</html>

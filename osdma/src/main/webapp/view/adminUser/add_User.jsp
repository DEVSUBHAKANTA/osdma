<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel='stylesheet'
	href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'>

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="/assets/coustomjs/userJquery.js" type="text/javascript"></script>
<!-- <script src="/assets/coustomjs/userJquery.js"></script> -->
</head>
<body>
<%@include file="adminNavigation.jsp"%>
	<div class="container">
		<h1>Add Block user</h1>
		<hr>
		<div class="row">
			<!-- left column -->
			<div class="col-md-3"></div>

			<!-- edit form column -->
			<div class="col-md-6 personal-info">
				<div class="alert alert-info alert-dismissable">
					<a class="panel-close close" data-dismiss="alert">�</a> <i
						class="fa fa-coffee"></i> This is an <strong>.alert</strong>
					<div id="statusmsg"></div>
				</div>
				<h3>User info</h3>

				<form class="form-horizontal" role="form" onsubmit="return false">
					<div class="form-group">
						<label class="col-lg-3 control-label">User Code:</label>
						<div class="col-lg-8">
							<input class="form-control" type="text" id="usercode" value="">
							<span class="errorMsg" id="errorusercode">*Please provide the valid usercode.</span>
						</div>
					</div>
						<div class="form-group">
						
						<label class="col-lg-3 control-label">Role</label>
						<div class="col-lg-8">
							<select  class="form-control" name="districtName" id="role">
								<option value="">Select Role</option>
								<option value="ADMIN">ADMIN</option>
								<option value="HQ">HQ</option>
								<option value="USER">USER</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Email:</label>
						<div class="col-lg-8">
							<input class="form-control" type="email" id="email" value=""
								required>
							<span class="errorMsg" id="erroremail">*Please provide the valid email.</span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Phone Number:</label>
						<div class="col-lg-8">
							<input class="form-control" type="number" id="phone" value=""
								required>
							<span class="errorMsg" id="errormobile">*Please provide the valid phone number.</span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">password:</label>
						<div class="col-lg-8">
							<input class="form-control" type="password" id="password"
								value="" required>
							<span class="errorMsg" id="errorpassword">*Please provide the valid password.</span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">First Name:</label>
						<div class="col-lg-8">
							<input class="form-control" type="text" id="fname" value=""
								required>
								<span class="errorMsg" id="errorfirstname">*Please provide the valid Name.</span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Middle Name:</label>
						<div class="col-lg-8">
							<input class="form-control" type="text" id="mname" value=""
								required>
								<span class="errorMsg" id="errormiddlename">*Please provide the valid Middle Name.</span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Last Name:</label>
						<div class="col-lg-8">
							<input class="form-control" type="text" id="lname" value=""
								required>
								<span class="errorMsg" id="errorlastname">*Please provide the valid Last Name.</span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-lg-3 control-label">District Name:</label>
						<div class="col-lg-8">
							<input class="form-control" type="text" id="dist" value=""
								required>
								<span class="errorMsg" id="errordistrict">*Please provide the valid District name.</span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Block Name:</label>
						<div class="col-lg-8">
							<input class="form-control" type="text" id="block" value=""
								required>
								<span class="errorMsg" id="errorblock">*Please provide the valid Block.</span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Panchayat Name:</label>
						<div class="col-lg-8">
							<input class="form-control" type="text" id="panchayat" value=""
								required>
								<span class="errorMsg" id="errorpanchayat">*Please provide the valid panchayat.</span>
						</div>
					</div>
					
	
					<!-- <div class="form-group">
 						<label style="margin-left: 50%;" class="radio-inline"><input type="radio" 
 							name="optradio" checked>Head Quarter</label> <label
 							class="radio-inline"><input type="radio" name="optradio">Block</label>
							<span class="errorMsg" id="errorpanchayat">*Please choose one.</span>
 					</div> -->
					<div class="form-group">
						<label class="col-md-3 control-label"></label>
						<div class="col-md-8">
							<input type="submit" class="btn btn-primary" id="btnSave"
								value="Save"> <span></span> <input type="reset"
								class="btn btn-default" value="Cancel">
						</div>
					</div>
				</form>
			</div>
			<div class="col-md-3"></div>
		</div>
	</div>
	<%@include file="../common/Footer.jsp"%>
</body>
</html>
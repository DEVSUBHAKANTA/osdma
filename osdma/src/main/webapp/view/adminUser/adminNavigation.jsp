
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-6 col-xs-6">
			<a class="logo" href=""> <img src="../assets/images/DQSLogo.png"
				width="110" height="85" alt="logo"> <!-- 				<h1>hello</h1> -->

			</a>
		</div>
		<div class="col-sm-6 col-xs-6">
			<div class="w3-right w3-hide-small w3-wide toptext"
				style="font-family: 'Segoe UI', Arial, sans-serif">
				<h4>
					ODISHA STATE DISASTER MANAGEMENT AUTHORITY<br> <small
						style="color: blue">Data Query System</small>
				</h4>
			</div>
		</div>
	</div>
</div>
<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#myNavbar">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/nav"> <i class="fa fa-home"></i></a>
		</div>

		<div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav">
				<li><a href="#">Dashboard </a></li>
				<li><a href="/admin/realtime">Real Time Data </a></li>
				<li><a href="/admin/Historical">Historical Data </a></li>
				
				<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Admin Privilege <span class="caret"></span></a>
					
					<ul class="dropdown-menu">
						<li><a href="/admin/add_new_station">Create station</a></li>
						<li><a href="/admin/update_station">Update station</a></li>
						<li><a href="/admin/add_new_user">Create user</a></li>
						<li><a href="/admin/update_user">Update user</a></li>
					</ul></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">				
				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#"><span class='fas fa-cog'></span><span
						class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="/admin/Profile">Profile</a></li>
						<li><a href="#" data-toggle="modal" data-target="#myModal">Change
								Password</a></li>
						<li><a href="/logout">Log Out</a></li>
					</ul></li>
			</ul>
		</div>
	</div>
</nav>

<div class="container">
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Change Password</h4>
				</div>
				<div class="modal-body">

					<div class="form-group" id="currentPass-group">
						<label for="current_pass">Current Password :</label> <input
							class="form-control" type="password" name="current_pass"
							id="current_pass">
					</div>
					<div class="form-group">
						<label for="new_pass">New Password:</label> <input
							class="form-control" type="password" name="new_pass"
							id="new_pass">
					</div>

					<div class="form-group">
						<label for="confirm_pass">Confirm Password :</label> <input
							class="form-control" type="password" name="confirm_pass"
							id="confirm_pass">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary">Save</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
</div>


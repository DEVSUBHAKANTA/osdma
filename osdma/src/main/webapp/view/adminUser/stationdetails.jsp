<%-- 
    Document   : stationdetails
    Created on : Jul 21, 2019, 12:59:37 PM
    Author     : BIKASH
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>osdma add station</title>
        <link href="assets/css/bootstrap.min-3.3.6.css" rel="stylesheet">
        <script src="assetsjs/jquery-1.12.1.min.js"></script>
        <script src="assets/js/bootstrap.min-3.3.6.js"></script>
    </head>
    <body>
        <div>    
            <div class="row content">
                <div class="col-sm-2 sidenav">
                    <!-- <p><a href="#">Link</a></p>
                    <p><a href="#">Link</a></p>
                    <p><a href="#">Link</a></p> -->
                </div>
                <div class="col-sm-8 text-left"> 
                    <p>
                    <h2><b>Add Station : </b></h2>
							</p>

                    <form role="form" class="form-horizontal">
                        <div class="col-md-12" style="margin-top:3%">

                            <div class="box-body" id="empAddrsForm" style="background-color:white"><br>
                                <%-- <form class="form-horizontal"> --%>
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="districtname" class="col-sm-2 control-label">District Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control"
                                                   id="districtname" placeholder="District Name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="blockname" class="col-sm-2 control-label">Block Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control"
                                                   id="blockname" placeholder="Block Name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="StationName" class="col-sm-2 control-label">Station Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control"
                                                   id="StationName" placeholder="Station Name">
                                        </div>
                                    </div>
                                    <div class="form-group">                                        
                                        <label for="Dateofcreation" class="col-sm-2 control-label">Date of creation</label>
                                        <div class="col-sm-10">
                                            <input type="tel" class="form-control"
                                                   id="Dateofcreation" placeholder="Date of creation">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="ImeiNumber" class="col-sm-2 control-label">Imei Number</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control"
                                                   id="ImeiNumber" placeholder="Imei Number">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="Latitude" class="col-sm-2 control-label">Latitude</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control"
                                                   id="Latitude" placeholder="Locality">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="Longitude" class="col-sm-2 control-label">Longitude</label>
                                        <div class="col-sm-10">
                                            <input type="number" class="form-control"
                                                   id="Longitude" placeholder="Longitude">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="stationtypes" class="col-sm-2 control-label">Station types</label>

                                        <div class="col-sm-10">
                                            <input type="tel" class="form-control"
                                                   id="stationtypes" placeholder="Station types">
                                        </div>
                                    </div>
                                    <div class="form-group pull-right">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" class="btn btn-primary" id="btnEmpAddrs">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
                <div class="col-sm-2 sidenav">
<!--                     <div class="well"> -->
<!--                         <p>ADS</p> -->
<!--                     </div> -->
<!--                     <div class="well"> -->
<!--                         <p>ADS</p> -->
<!--                     </div> -->
                </div>
            </div>
        </div>

        <footer class="container-fluid text-center">
            <p>Footer Text</p>
        </footer>


    </body>
</html>

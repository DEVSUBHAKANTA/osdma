<!DOCTYPE html>
<html lang="en">
<head>
<title>Bootstrap Example</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- font-awesome library -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- font-awesome icon library-->
<link rel='stylesheet'
	href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

<link rel="stylesheet"
	href="assets/datetimepicker-master/build/jquery.datetimepicker.min.css">
<!-- jQuery library -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script
	src="assets/datetimepicker-master/build/jquery.datetimepicker.full.js"></script>
<script
	src="assets/datetimepicker-master/build/jquery.datetimepicker.full.min.js"></script>
<script
	src="assets/datetimepicker-master/build/jquery.datetimepicker.min.js"></script>

<!-- Bootstrap library -->
 <!-- <script>
jQuery(function(){
	 jQuery('#date_timepicker_start').datetimepicker({
	  format:'Y/m/d',
	  onShow:function( ct ){
	   this.setOptions({
	    maxDate:jQuery('#date_timepicker_end').val()?jQuery('#date_timepicker_end').val():false
	   })
	  },
	  timepicker:false
	 });
	 jQuery('#date_timepicker_end').datetimepicker({
	  format:'Y/m/d',
	  onShow:function( ct ){
	   this.setOptions({
	    minDate:jQuery('#date_timepicker_start').val()?jQuery('#date_timepicker_start').val():false
	   })
	  },
	  timepicker:false
	 });
	});
</script> -->
 <script type="text/javascript">
	$(document).ready(function() {
		$('#date_timepicker_start').datetimepicker();
		format: 'dd-M-yyyy hh:ii'

		$('#date_timepicker_end').datetimepicker();
				
	});
</script>
</head>
<body>



	<%@include file="adminNavigation.jsp"%>
	
	<div class="container-fluid">
		<!-- 	............drop down start.................. -->
		<div class="table-responsive">
			<table class="table">
				<thead>
					<tr>

						<th><span class="text-primary">District:</span> <select
							class="form-control">
								<option value="volvo">select District</option>
								<option value="saab">Anugul</option>
								<option value="opel">Khordha</option>
								<option value="audi">puri</option>
						</select></th>
						<th><span class="text-primary"> Block:</span> <select
							class="form-control">
								<option value="volvo">select District</option>
								<option value="saab"></option>
								<option value="opel">Khordha</option>
								<option value="audi">puri</option>
						</select></th>
						<th><span class="text-primary"> Panchayat:</span> <select
							class="form-control">
								<option value="volvo">select District</option>
								<option value="saab">Anugul</option>
								<option value="opel">Khordha</option>
								<option value="audi">puri</option>
						</select></th>

						<th><span class="text-primary"> Begin Date:</span>

							<div class='input-group date' >
								<input type='text' class="form-control" id='date_timepicker_start'/> <span
									class="input-group-addon"> <span
									class="glyphicon glyphicon-calendar"></span>
								</span>
							</div></th>
						<th><span class="text-primary"> End Date:</span>
							<div class='input-group date' >
								<input type='text' class="form-control" id='date_timepicker_end'/> <span
									class="input-group-addon"> <span
									class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						<th><button class="btn btn-success">
								<i class="fa fa-search" aria-hidden="true"></i>
							</button></th>
					</tr>
				</thead>
				<tbody>

				</tbody>
			</table>
		</div>
		<!--   <hr style="background-color: green; height: 1px; border: 0;"> -->
		<!--   .............drop down end........... -->
		<!--   .............data tables start................... -->
		<div class="container-fluid">
			<ul class="nav nav-tabs ">
				<li class="nav-item"><a class="btn btn-info active"
					data-toggle="tab" href="#home"><i class="fa fa-line-chart"></i>&nbsp;Chart</a>
				</li>
				<li class="nav-item"><a class="btn btn-info"
					data-toggle="tab" href="#menu1"><i class="fas fa-th"></i>&nbsp;Grid</a>
				</li>
				<li>&nbsp;&nbsp; <input type="text" id="myInput" onkeyup="myFunction()"
					placeholder="Search for names.." title="Type in a name">
				<button type="submit" class="btn btn-success"><i class="fa fa-search"></i>
					
				</button></li>
				
				<li class="btn-group pull-right">
				<button type="button" class="btn btn-primary">5mins</button>
				<button type="button" class="btn btn-primary">1hrs</button>
				<button type="button" class="btn btn-primary">Daily</button>
			</li>
			</ul>				

			</div>
			<form>
			<!-- <div class="input-group">
						<input type="text" name="Name" class="form-control"
							placeholder="Search By Name . . . ." id="myInput" onkeyup="myFunction()">

						<span class="input-group-btn">
							<button name="search" id="btnSearch" class="btn btn-flat">
								<i class="fa fa-search"></i>
							</button>
						</span>

					</div> -->
<!-- 				&nbsp;&nbsp; <input type="text" id="myInput" onkeyup="myFunction()" -->
<!-- 					placeholder="Search for names.." title="Type in a name"> -->
<!-- 				<button type="submit" class="btn btn-success"> -->
<!-- 					<i class="fa fa-search"></i> -->
<!-- 				</button> -->
			</form>
		</div>
		<div class="table-responsive">
			<table class="table table-bordered table-striped"
				style="border-color: green;" id="myTable">
				<thead>
					<tr>
						<th>slno</th>
						<th>station code</th>
						<th>stname</th>

					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>001</td>
						<td>haripur</td>
					</tr>
					<tr>
						<td>2</td>
						<td>002</td>
						<td>jatni</td>
					</tr>
					<tr>
						<td>3</td>
						<td>003</td>
						<td>jagamara</td>
					</tr>
					<tr>
						<td>4</td>
						<td>004</td>
						<td>pipili</td>
					</tr>
					<tr>
						<td>5</td>
						<td>005</td>
						<td>khordha</td>
					</tr>
					<tr>
						<td>6</td>
						<td>006</td>
						<td>puri</td>
					</tr>
					<tr>
						<td>7</td>
						<td>007</td>
						<td>nimapara</td>
					</tr>
					<tr>
						<td>8</td>
						<td>008</td>
						<td>matiapara</td>
					</tr>
					<tr>
						<td>8</td>
						<td>008</td>
						<td>matiapara</td>
					</tr>
					<tr>
						<td>8</td>
						<td>008</td>
						<td>matiapara</td>
					</tr>
					<tr>
						<td>8</td>
						<td>008</td>
						<td>matiapara</td>
					</tr>
					<tr>
						<td>8</td>
						<td>008</td>
						<td>matiapara</td>
					</tr>
					<tr>
						<td>8</td>
						<td>008</td>
						<td>matiapara</td>
					</tr>
					<tr>
						<td>8</td>
						<td>008</td>
						<td>matiapara</td>
					</tr>
					<tr>
						<td>8</td>
						<td>008</td>
						<td>matiapara</td>
					</tr>
					<tr>
						<td>8</td>
						<td>008</td>
						<td>matiapara</td>
					</tr>
					<tr>
						<td>8</td>
						<td>008</td>
						<td>matiapara</td>
					</tr>
					<tr>
						<td>8</td>
						<td>008</td>
						<td>matiapara</td>
					</tr>
					<tr>
						<td>8</td>
						<td>008</td>
						<td>matiapara</td>
					</tr>
					<tr>
						<td>8</td>
						<td>008</td>
						<td>matiapara</td>
					</tr>
					<tr>
						<td>8</td>
						<td>008</td>
						<td>matiapara</td>
					</tr>
					<tr>
						<td>8</td>
						<td>008</td>
						<td>matiapara</td>
					</tr><tr>
						<td>8</td>
						<td>008</td>
						<td>matiapara</td>
					</tr>
					<tr>
						<td>8</td>
						<td>008</td>
						<td>matiapara</td>
					</tr>
					<tr>
						<td>8</td>
						<td>008</td>
						<td>matiapara</td>
					</tr>
					<tr>
						<td>8</td>
						<td>008</td>
						<td>matiapara</td>
					</tr>
				</tbody>
			</table>

			<script>
				function myFunction() {
					var input, filter, table, tr, td, i, txtValue;
					input = document.getElementById("myInput");
					filter = input.value.toUpperCase();
					table = document.getElementById("myTable");
					tr = table.getElementsByTagName("tr");
					for (i = 0; i < tr.length; i++) {
						td = tr[i].getElementsByTagName("td")[2];
						if (td) {
							txtValue = td.textContent || td.innerText;
							if (txtValue.toUpperCase().indexOf(filter) > -1) {
								tr[i].style.display = "";
							} else {
								tr[i].style.display = "none";
							}
						}
					}
				}
			</script>

		</div>
		<!--   .............data tables end................... -->
		<nav class="pagination pull-right">
			<ul class="pagination pagination-sm">
				<li class="page-item disabled"><span class="page-link"><i
						class='fas fa-chevron-left'></i></span></li>
				<li class="page-item active"><a href="#" class="page-link">1</a></li>
				<li class="page-item"><a href="#" class="page-link">2</a></li>
				<li class="page-item"><a href="#" class="page-link">3</a></li>
				<li class="page-item"><a href="#" class="page-link">4</a></li>
				<li class="page-item"><a href="#" class="page-link">5</a></li>
				<li class="page-item"><a href="#" class="page-link"> <i
						class='fas fa-chevron-right'></i></a></li>

			</ul>
		</nav>
	</div>
	<%@include file="../common/Footer.jsp" %>
</body>
<link rel="stylesheet" type="text/css" href="assets/datetimepicker-master/build/jquery.datetimepicker.min.css"/>
<script src="assets/datetimepicker-master/jquery.datetimepicker.js"></script>
</html>

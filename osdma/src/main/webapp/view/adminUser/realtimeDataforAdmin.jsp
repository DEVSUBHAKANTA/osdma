<!DOCTYPE html>
<html lang="en">
<head>
<title>Bootstrap Example</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<link rel='stylesheet'
	href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'>
	
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  
  
</head>
<body>
	<%@include file="adminNavigation.jsp"%>

	<div class="container-fluid">
		<!--............drop down start.................. -->
		<div class="table-responsive">
			<table class="table">
				<thead>
					<tr>

						<th><span class="text-primary">District:</span> <select
							class="form-control">
								<option value="volvo">select District</option>
								<option value="saab">Anugul</option>
								<option value="opel">Khordha</option>
								<option value="audi">puri</option>
						</select></th>
						<th><span class="text-primary"> Block:</span> <select
							class="form-control">
								<option value="volvo">select District</option>
								<option value="saab"></option>
								<option value="opel">Khordha</option>
								<option value="audi">puri</option>
						</select></th>
						<th><span class="text-primary"> Panchayat:</span> <select
							class="form-control">
								<option value="volvo">select District</option>
								<option value="saab">Anugul</option>
								<option value="opel">Khordha</option>
								<option value="audi">puri</option>
						</select></th>
						<!-- <th><span class="text-primary">District:</span> <select
							class="form-control">
								<option value="volvo">select District</option>
								<option value="saab">Anugul</option>
								<option value="opel">Khordha</option>
								<option value="audi">puri</option>
						</select></th> -->
						<th><button class="btn btn-success">
								<i class="fa fa-search" aria-hidden="true"></i>
							</button></th>
					</tr>
				</thead>
				<tbody>

				</tbody>
			</table>
		</div>
		<!--   <hr style="background-color: green; height: 1px; border: 0;"> -->
		<!--   .............drop down end........... -->

		<!-- --------------------Tabs------------------------- -->
		<div class="container-fluid">
			<br>
			<!-- Nav tabs -->
			<ul class="nav nav-tabs ">
				<li class="nav-item"><a class="btn btn-info active"
					data-toggle="tab" href="#home"><i class="fa fa-line-chart"></i>&nbsp;Chart</a>
				</li>
				<li class="nav-item"><a class="btn btn-info"
					data-toggle="tab" href="#menu1"><i class="fas fa-th"></i>&nbsp;Grid</a>
				</li>
				
				<li class="btn-group pull-right">
				<button type="button" class="btn btn-primary">5mins</button>
				<button type="button" class="btn btn-primary">1hrs</button>
			</li>
			</ul>

			<!-- 			<ul class="nav nav-tabs pull-right" role="tablist"> -->
			<!-- 				<li class="nav-item"><a class="btn btn-primary active" -->
			<!-- 					data-toggle="tab" href="#home"><i class="fa fa-line-chart"></i>&nbsp;Chart</a> -->
			<!-- 				</li> -->
			<!-- 				<li class="nav-item"><a class="btn btn-primary" -->
			<!-- 					data-toggle="tab" href="#menu1"><i class="fas fa-th"></i>&nbsp;Grid</a> -->
			<!-- 				</li> -->
			<!-- 			</ul> -->

			<!-- Tab panes -->
			<div class="tab-content">
				<div id="home" class="container-fluid tab-pane active">
					<br>
					<div class="tab-content table-responsive">
						<table class="table table-bordered" style="border-color: green;">
							<thead>
								<tr>
									<th>slno</th>
									<th>station code</th>
									<th>stname</th>
									<th>p1</th>
									<th>p2</th>
									<th>p3</th>
									<th>p4</th>
									<th>p5</th>
									<th>p6</th>
									<th>p7</th>
									<th>p8</th>
									<th>p9</th>
									<th>p10</th>
									<th>p11</th>
									<th>p12</th>

								</tr>
							</thead>
							<tbody>
								<tr>
									<td>1</td>
									<td>0.645</td>
									<td>046</td>
									<td>055</td>
									<td>045</td>
									<td>0.09</td>
									<td>67.9</td>
									<td>0.99</td>
									<td>0.8</td>
									<td>0.0</td>
									<td>0.0</td>
									<td>0.987</td>
									<td>0.86</td>
									<td>0.96</td>
									<td>0.98</td>
								</tr>
								<tr>
									<td>1</td>
									<td>0.645</td>
									<td>046</td>
									<td>055</td>
									<td>045</td>
									<td>0.09</td>
									<td>67.9</td>
									<td>0.99</td>
									<td>0.8</td>
									<td>0.0</td>
									<td>0.0</td>
									<td>0.987</td>
									<td>0.86</td>
									<td>0.96</td>
									<td>0.98</td>
								</tr>
								<tr>
									<td>1</td>
									<td>0.645</td>
									<td>046</td>
									<td>055</td>
									<td>045</td>
									<td>0.09</td>
									<td>67.9</td>
									<td>0.99</td>
									<td>0.8</td>
									<td>0.0</td>
									<td>0.0</td>
									<td>0.987</td>
									<td>0.86</td>
									<td>0.96</td>
									<td>0.98</td>
								</tr>
								<tr>
									<td>1</td>
									<td>0.645</td>
									<td>046</td>
									<td>055</td>
									<td>045</td>
									<td>0.09</td>
									<td>67.9</td>
									<td>0.99</td>
									<td>0.8</td>
									<td>0.0</td>
									<td>0.0</td>
									<td>0.987</td>
									<td>0.86</td>
									<td>0.96</td>
									<td>0.98</td>
								</tr>
								<tr>
									<td>1</td>
									<td>0.645</td>
									<td>046</td>
									<td>055</td>
									<td>045</td>
									<td>0.09</td>
									<td>67.9</td>
									<td>0.99</td>
									<td>0.8</td>
									<td>0.0</td>
									<td>0.0</td>
									<td>0.987</td>
									<td>0.86</td>
									<td>0.96</td>
									<td>0.98</td>
								</tr>
								<tr>
									<td>1</td>
									<td>0.645</td>
									<td>046</td>
									<td>055</td>
									<td>045</td>
									<td>0.09</td>
									<td>67.9</td>
									<td>0.99</td>
									<td>0.8</td>
									<td>0.0</td>
									<td>0.0</td>
									<td>0.987</td>
									<td>0.86</td>
									<td>0.96</td>
									<td>0.98</td>
								</tr>
								<tr>
									<td>1</td>
									<td>0.645</td>
									<td>046</td>
									<td>055</td>
									<td>045</td>
									<td>0.09</td>
									<td>67.9</td>
									<td>0.99</td>
									<td>0.8</td>
									<td>0.0</td>
									<td>0.0</td>
									<td>0.987</td>
									<td>0.86</td>
									<td>0.96</td>
									<td>0.98</td>
								</tr>
								<tr>
									<td>1</td>
									<td>0.645</td>
									<td>046</td>
									<td>055</td>
									<td>045</td>
									<td>0.09</td>
									<td>67.9</td>
									<td>0.99</td>
									<td>0.8</td>
									<td>0.0</td>
									<td>0.0</td>
									<td>0.987</td>
									<td>0.86</td>
									<td>0.96</td>
									<td>0.98</td>
								</tr>
								<tr>
									<td>1</td>
									<td>0.645</td>
									<td>046</td>
									<td>055</td>
									<td>045</td>
									<td>0.09</td>
									<td>67.9</td>
									<td>0.99</td>
									<td>0.8</td>
									<td>0.0</td>
									<td>0.0</td>
									<td>0.987</td>
									<td>0.86</td>
									<td>0.96</td>
									<td>0.98</td>
								</tr>
								<tr>
									<td>1</td>
									<td>0.645</td>
									<td>046</td>
									<td>055</td>
									<td>045</td>
									<td>0.09</td>
									<td>67.9</td>
									<td>0.99</td>
									<td>0.8</td>
									<td>0.0</td>
									<td>0.0</td>
									<td>0.987</td>
									<td>0.86</td>
									<td>0.96</td>
									<td>0.98</td>
								</tr>
								<tr>
									<td>1</td>
									<td>0.645</td>
									<td>046</td>
									<td>055</td>
									<td>045</td>
									<td>0.09</td>
									<td>67.9</td>
									<td>0.99</td>
									<td>0.8</td>
									<td>0.0</td>
									<td>0.0</td>
									<td>0.987</td>
									<td>0.86</td>
									<td>0.96</td>
									<td>0.98</td>
								</tr>
								<tr>
									<td>1</td>
									<td>0.645</td>
									<td>046</td>
									<td>055</td>
									<td>045</td>
									<td>0.09</td>
									<td>67.9</td>
									<td>0.99</td>
									<td>0.8</td>
									<td>0.0</td>
									<td>0.0</td>
									<td>0.987</td>
									<td>0.86</td>
									<td>0.96</td>
									<td>0.98</td>
								</tr>
								<tr>
									<td>1</td>
									<td>0.645</td>
									<td>046</td>
									<td>055</td>
									<td>045</td>
									<td>0.09</td>
									<td>67.9</td>
									<td>0.99</td>
									<td>0.8</td>
									<td>0.0</td>
									<td>0.0</td>
									<td>0.987</td>
									<td>0.86</td>
									<td>0.96</td>
									<td>0.98</td>
								</tr>
								<tr>
									<td>1</td>
									<td>0.645</td>
									<td>046</td>
									<td>055</td>
									<td>045</td>
									<td>0.09</td>
									<td>67.9</td>
									<td>0.99</td>
									<td>0.8</td>
									<td>0.0</td>
									<td>0.0</td>
									<td>0.987</td>
									<td>0.86</td>
									<td>0.96</td>
									<td>0.98</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div id="menu1" class="container-fluid">
					<br>
				</div>
				<div id="menu2" class="container-fluid">
					<br>
					<div id="container"
						style="min-width: 310px; height: 400px; margin: 0 auto">hello</div>
				</div>
			</div>
		</div>

		<!-- <div class="container-fluid">
			<div class="btn-group pull-left">
				<button type="button" class="btn btn-info">
					<i class="fa fa-line-chart"></i>&nbsp;Chart
				</button>
				<button type="button" class="btn btn-info">
					<i class="fas fa-th"></i>&nbsp;Grid
				</button>
			</div>

			<div class="btn-group pull-right">
				<button type="button" class="btn btn-primary">5mins</button>
				<button type="button" class="btn btn-primary">1hrs</button>
			</div>
		</div> -->
		<!-- 		<div class="tab-content table-responsive"> -->
		<!-- 			<table class="table table-bordered" style="border-color: green;"> -->
		<!-- 				<thead> -->
		<!-- 					<tr> -->
		<!-- 						<th>slno</th> -->
		<!-- 						<th>station code</th> -->
		<!-- 						<th>stname</th> -->
		<!-- 						<th>p1</th> -->
		<!-- 						<th>p2</th> -->
		<!-- 						<th>p3</th> -->
		<!-- 						<th>p4</th> -->
		<!-- 						<th>p5</th> -->
		<!-- 						<th>p6</th> -->
		<!-- 						<th>p7</th> -->
		<!-- 						<th>p8</th> -->
		<!-- 						<th>p9</th> -->
		<!-- 						<th>p10</th> -->
		<!-- 						<th>p11</th> -->
		<!-- 						<th>p12</th> -->

		<!-- 					</tr> -->
		<!-- 				</thead> -->
		<!-- 				<tbody> -->
		<!-- 					<tr> -->
		<!-- 						<td>1</td> -->
		<!-- 						<td>0.645</td> -->
		<!-- 						<td>046</td> -->
		<!-- 						<td>055</td> -->
		<!-- 						<td>045</td> -->
		<!-- 						<td>0.09</td> -->
		<!-- 						<td>67.9</td> -->
		<!-- 						<td>0.99</td> -->
		<!-- 						<td>0.8</td> -->
		<!-- 						<td>0.0</td> -->
		<!-- 						<td>0.0</td> -->
		<!-- 						<td>0.987</td> -->
		<!-- 						<td>0.86</td> -->
		<!-- 						<td>0.96</td> -->
		<!-- 						<td>0.98</td> -->
		<!-- 					</tr> -->
		<!-- 				</tbody> -->
		<!-- 			</table> -->
		<!-- 		</div> -->
		<!--   .............data tables end................... -->
		<nav class="pagination pull-right">
			<ul class="pagination pagination-sm">
				<li class="page-item disabled"><span class="page-link">Previous</span></li>
				<li class="page-item active"><a href="#" class="page-link">1</a></li>
				<li class="page-item"><a href="#" class="page-link">2</a></li>
				<li class="page-item"><a href="#" class="page-link">3</a></li>
				<li class="page-item"><a href="#" class="page-link">4</a></li>
				<li class="page-item"><a href="#" class="page-link">5</a></li>
				<li class="page-item"><a href="#" class="page-link">Next</a></li>
			</ul>
		</nav>
	</div>
	<%@include file="../common/Footer.jsp"%>
</body>

</html>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel='stylesheet'
	href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="/assets/coustomjs/stationJquery.js"></script>

</head>
<body>
	<%@include file="adminNavigation.jsp"%>
	<div class="container">
		<div class="row">
			<!-- left column -->
			<div class="col-sm-3">
				<!-- edit form column -->
				<!-- <div class="alert alert-success">
  <strong>Success!</strong> Indicates a successful or positive action.-->
			</div>
			<div class="col-md-6">
				<h2>Add New Station...</h2>
				<hr>
				
					<div class="alert alert-info" id="divSuccessAlert">
						<a class="panel-close close" data-dismiss="alert">�</a><i
							class="fa fa-check" aria-hidden="true"></i><strong>.Success</strong>.<span
							id="successdiv"></span>

					</div>
					<div class="alert alert-danger" id="divDangerAlert">
						<a class="panel-close close" data-dismiss="alert">�</a><i
							class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>Failed</strong>.<span
							id="alertdiv"></span>

					</div>
				
				<form class="form-horizontal" role="form" id="formStation"
					onsubmit="return false" autocomplete="off">


					<div class="form-group">
						<label class="col-sm-4 control-label">Station Code:</label>
						<div class="col-sm-8">
							<input class="form-control" type="text" value=""
								name="stationCode" id="stationCode" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">Station Name:</label>
						<div class="col-sm-8">
							<input class="form-control" type="text" value=""
								name="stationName" id="stationName" required>
						</div>
					</div>

					<div class="form-group">

						<label class="col-sm-4 control-label">Station Type:</label>
						<div class="col-sm-8">
							<select class="form-control" name="districtName" id="stationType">
								<option value="">Select Station Type:</option>
								<option value="AWS">AWS</option>
								<option value="ARG_A">ARG_A</option>
								<option value="ARG_BC">ARG_BC</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-4 control-label">Date Of Creation:</label>
						<div class="col-sm-8">
							<input class="form-control" type="Date" name="creationDate"
								id="creationDate">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">Imei Number:</label>
						<div class="col-sm-8">
							<input class="form-control" type="text" value=""
								name="stationImei" id="stationImei" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">Latitude:</label>
						<div class="col-sm-8">
							<input class="form-control" type="text" name="latitude"
								id="latitude" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">Longitude:</label>
						<div class="col-sm-8">
							<input class="form-control" type="text" name="longitude"
								id="longitude" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">District Name:</label>
						<div class="col-sm-8">
							<select class="form-control" name="districtName"
								id="districtName">
								<option value="">Select District</option>
								<option value="Angul">Angul</option>
								<option value="Balangir">Balangir</option>
								<option value="Balasore">Balasore</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">Block Name:</label>
						<div class="col-sm-8">
							<select class="form-control" name="blockName" id="blockName">
								<option value="Angul">Angul</option>
								<option value="Athmallik">Athmallik</option>
								<option value="Banarpal">Banarpal</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">Panchayat Name:</label>
						<div class="col-sm-8">
							<select class="form-control" name="panchayatName"
								id="panchayatName">
								<option value="Angarbandha">Angarbandha</option>
								<option value="Antulia">Antulia</option>
								<option value="Badakantakul">Badakantakul</option>
							</select>
						</div>
					</div>



					<div class="form-group">
						<label class="col-sm-4 control-label"></label>
						<div class="col-sm-8">
							<!-- 	<input type="submit" class="btn btn-primary" value="Save" id="addStation"> -->
							<button type="submit" id="submitBtn"
								class="btn btn-primary btn-sm">Submit</button>
							<span></span> <input type="reset" class="btn btn-default"
								value="Cancel">
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="col-md-3"></div>
	</div>
	<%@include file="../common/Footer.jsp"%>
</body>
</html>
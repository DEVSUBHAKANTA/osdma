	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-6 col-xs-6">
				<a class="logo" href=""> <img src="../assets/images/DQSLogo.png" width="110"
					height="85" alt="logo"> <!-- 				<h1>hello</h1> -->

				</a>
			</div>
			<div class="col-sm-6 col-xs-6">
				<div class="w3-right w3-hide-small w3-wide toptext"
					style="font-family: 'Segoe UI', Arial, sans-serif">
					<h4>
						ODISHA STATE DISASTER MANAGEMENT AUTHORITY<br> <small
							style="color: blue">Data Query System</small>
					</h4>
				</div>
			</div>
		</div>
	</div>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"> <i class="fa fa-home"></i></a>
			</div>
			
			<div class="collapse navbar-collapse" id="myNavbar">
				<ul class="nav navbar-nav">
					<li><a href="#">Dashboard </a></li>
					<li><a href="/realtime">Real Time Data </a></li>
					<li><a href="/Historical">Historical Data </a></li>
				</ul>
				
				<ul class="nav navbar-nav navbar-right">
					<!--         <li><a href="#"><span class="glyphicon glyphicon-user"></span> Setting</a></li> -->
					<li class="dropdown"><a class="dropdown-toggle"
						data-toggle="dropdown" href="#"><span class='fas fa-cog'></span><span
							class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#" data-toggle="modal" data-target="#myModal1">MyProfile</a></li>
							<li><a href="#">Log Out</a></li>
						</ul></li>
				</ul>
			</div>
		</div>
	</nav>

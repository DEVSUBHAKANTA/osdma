<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<title>Bootstrap Example</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/login.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<link rel='stylesheet'
	href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'>

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-6 col-xs-6">
				<a class="logo" href=""> <img src="../assets/images/DQSLogo.png" width="110"
					height="85" alt="logo"> <!-- 				<h1>hello</h1> -->

				</a>
			</div>
			<div class="col-sm-6 col-xs-6">
				<div class="w3-right w3-hide-small w3-wide toptext"
					style="font-family: 'Segoe UI', Arial, sans-serif">
					<h4>
						ODISHA STATE DISASTER MANAGEMENT AUTHORITY<br> <small
							style="color: blue">Data Query System</small>
					</h4>
				</div>
			</div>
			
			<h1>Hello Block user</h1>
			<!--  <img src="images (1).jpg" class="img-rounded" alt="osdma" width="110" height="90"> -->
		</div>
	</div>

	<%@include file="blockUserNavigation.jsp" %>
</body>
</html>
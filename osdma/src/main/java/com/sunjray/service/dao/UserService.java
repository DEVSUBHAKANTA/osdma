package com.sunjray.service.dao;

import com.sunjray.json.ExternalUser;
import com.sunjray.model.User;

public interface UserService {

	User saveUser(ExternalUser user);
}

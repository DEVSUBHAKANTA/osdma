package com.sunjray.service.dao;

import java.util.List;

import com.sunjray.json.ExternalStation;
import com.sunjray.model.Station;
public interface StationService {

	Station saveStation(ExternalStation station);
	Station editStation(ExternalStation station,String id);
	Boolean deleteStation(String id);
	List<Station> getAllStations();
	Station getStation(String id);
	
}

/*
 * package com.sunjray.service.daoImpl;
 * 
 * import java.util.List; import java.util.stream.Collectors;
 * 
 * import org.springframework.security.core.GrantedAuthority; import
 * org.springframework.security.core.authority.SimpleGrantedAuthority; import
 * com.sunjray.model.Role; import com.sunjray.model.User;
 * 
 * public class JwtUserFactory { private JwtUserFactory() { }
 * 
 * public static JwtUser create(User user) { return new
 * JwtUser(user.getUsername(), user.getFirstname(), user.getMiddlename(),
 * user.getLastname(), user.getPassword(),
 * mapToGrantedAuthorities(user.getRoles()), user.getLastPasswordResetDate(),
 * user.getEnabled()); }
 * 
 * 
 * private static List<GrantedAuthority> mapToGrantedAuthorities(List<Role>
 * authorities){ return authorities.stream() .map(authority -> new
 * SimpleGrantedAuthority(authority.getName())) .collect(Collectors.toList()); }
 * }
 */
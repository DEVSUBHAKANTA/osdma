
package com.sunjray.service.daoImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import com.sunjray.json.ExternalUser;
import com.sunjray.model.Location;
import com.sunjray.model.Role;
import com.sunjray.model.User;
import com.sunjray.repository.BlockRepository;
import com.sunjray.repository.DistrictRepository;
import com.sunjray.repository.PanchayatRepository;
import com.sunjray.repository.RoleRepository;
import com.sunjray.repository.UserRepository;
import com.sunjray.security.UserPrincipal;

@Service
public class JwtuserdetailsService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private PanchayatRepository panchayatRepository;

	@Autowired
	private BlockRepository blockRepository;

	@Autowired
	private DistrictRepository districtRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		 Optional<User> optionalUser = userRepository.findByUsername(username);
	        if(optionalUser.isPresent()) {
	        	User user=optionalUser.get();
	        	System.out.println(user.getUsername());
	        	System.out.println(user.getPassword());
	        	System.out.println(user.getRoles());
	        }
	        return Optional.ofNullable(optionalUser).orElseThrow(()->new UsernameNotFoundException("Username Not Found"))
	               .map(UserPrincipal::new).get();
	}
	
	public User saveUser(ExternalUser externalUser) {
		User userInstance = new User();
		Set<Role> roles = new HashSet<Role>();
		 Optional<User> optionalUser = userRepository.findByUsername(externalUser.getUsername());
		if (optionalUser.isPresent()) {
			 User userExists=optionalUser.get();
				System.out.println("inside if block" + userExists.getUsername());
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "USER ALREADY AVAILABLE");
		} else {
			userInstance.setUsername(externalUser.getUsername());
			userInstance.setPassword(bCryptPasswordEncoder.encode(externalUser.getPassword()));
			//userInstance.setPassword(externalUser.getPassword());
			userInstance.setEmail(externalUser.getEmail());
			userInstance.setEnabled(1);
			userInstance.setLastPasswordResetDate(new Date());
			userInstance.setFirstname(externalUser.getFirstname());
			userInstance.setLastname(externalUser.getLastname());
			userInstance.setMiddlename(externalUser.getMiddlename());
			userInstance.setPhoneNumber(externalUser.getPhoneNumber());

			Location locationInstance = new Location();

			/*
			 * locationInstance.setLatitude(externalUser.getLatitude());
			 * locationInstance.setLongitude(externalUser.getLongitude());
			 */

			locationInstance.setBlock(blockRepository.findByNameContainingIgnoreCase(externalUser.getBlockName()));
			locationInstance.setPanchayat(panchayatRepository.findByNameContainingIgnoreCase(externalUser.getPanchayatName()));
			locationInstance.setDistrict(districtRepository.findByNameContainingIgnoreCase(externalUser.getDistrictName()));
			System.out.println(locationInstance.getDistrict().getId());
			userInstance.setLocation(locationInstance);
			if (externalUser.getRoles() != null) {
				Role roleExists = roleRepository.findByNameContainingIgnoreCase(externalUser.getRoles());
				if (roleExists != null) {
					roles.add(roleExists);
				} else {
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ROLE NOT FOUND IN DB");
				}
			}
			userInstance.setRoles(roles);

		}
		return userRepository.save(userInstance);
	}

}

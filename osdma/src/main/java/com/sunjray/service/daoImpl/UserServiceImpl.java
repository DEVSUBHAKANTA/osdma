
package com.sunjray.service.daoImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.sunjray.json.ExternalUser;
import com.sunjray.model.Location;
import com.sunjray.model.User;
import com.sunjray.repository.BlockRepository;
import com.sunjray.repository.DistrictRepository;
import com.sunjray.repository.PanchayatRepository;
import com.sunjray.repository.RoleRepository;
import com.sunjray.repository.UserRepository;
import com.sunjray.service.dao.UserService;
import com.sunjray.model.Role;;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PanchayatRepository panchayatRepository;

	@Autowired
	private BlockRepository blockRepository;

	@Autowired
	private DistrictRepository districtRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Override
	public User saveUser(ExternalUser user) {

		return userRepository.save(externalToUser.apply(user));
	}

	Function<ExternalUser, User> externalToUser = p -> {
		User userInstance = new User();
		List<Role> roles = new ArrayList<Role>();
		userInstance.setUsername(p.getUsername());
		userInstance.setPassword(p.getPassword());
		userInstance.setEmail(p.getEmail());
		userInstance.setFirstname(p.getFirstname());
		userInstance.setLastname(p.getLastname());
		userInstance.setMiddlename(p.getLastname());
		userInstance.setPhoneNumber(p.getPhoneNumber());

		Location locationInstance = new Location();

		/*
		 * locationInstance.setLatitude(p.getLatitude());
		 * locationInstance.setLongitude(p.getLongitude());
		 */
		locationInstance.setBlock(blockRepository.findByNameContainingIgnoreCase(p.getBlockName()));
		locationInstance.setPanchayat(panchayatRepository.findByNameContainingIgnoreCase(p.getPanchayatName()));
		locationInstance.setDistrict(districtRepository.findByNameContainingIgnoreCase(p.getDistrictName()));
		System.out.println(locationInstance.getDistrict().getId());
		/* userInstance.setLocation(locationInstance); */

		/*
		 * if (p.getRoles() != null) {
		 * 
		 * for (int i = 0; i < p.getRoles().size(); i++) { Role roleInstance =
		 * roleRepository.findByNameContainingIgnoreCase(p.getRoles().get(i));
		 * roles.add(roleInstance); }
		 * 
		 * userInstance.setRoles(roles); }
		 */
		return userInstance;
	};
}

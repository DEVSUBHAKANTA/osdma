package com.sunjray.service.daoImpl;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.sunjray.json.ExternalStation;
import com.sunjray.model.Location;
import com.sunjray.model.Station;
import com.sunjray.repository.BlockRepository;
import com.sunjray.repository.DistrictRepository;
import com.sunjray.repository.LocationRepository;
import com.sunjray.repository.PanchayatRepository;
import com.sunjray.repository.StationRepository;
import com.sunjray.repository.StationTypeRepository;
import com.sunjray.service.dao.StationService;

@Service
public class StationServiceImpl implements StationService {

	@Autowired
	private StationRepository stationRepository;

	@Autowired
	private PanchayatRepository panchayatRepository;

	@Autowired
	private BlockRepository blockRepository;

	@Autowired
	private DistrictRepository districtRepository;

	@Autowired
	private StationTypeRepository stationTypeRepository;

	@Autowired
	private LocationRepository locationrepository;

	@Override
	public Station saveStation(ExternalStation station) {

		return stationRepository.save(externalToStationInfo.apply(station));
	}

	Function<ExternalStation, Station> externalToStationInfo = p -> {
		Station stationInstance = new Station();
		stationInstance.setStationCode(p.getStationCode());
		stationInstance.setStationName(p.getStationName());
		stationInstance.setCreationDate(p.getCreationDate());

		stationInstance.setStationCode(p.getStationCode());
		stationInstance.setStationImei(p.getStationImei());
		if (p.getStationType() != null) {
			stationInstance.setStationType(stationTypeRepository.findByNameContainingIgnoreCase(p.getStationType()));
		}
		Location locationInstance = new Location();
		locationInstance.setLatitude(p.getLatitude());
		locationInstance.setLongitude(p.getLongitude());

		locationInstance.setBlock(blockRepository.findByNameContainingIgnoreCase(p.getBlockName()));
		locationInstance.setPanchayat(panchayatRepository.findByNameContainingIgnoreCase(p.getPanchayatName()));
		locationInstance.setDistrict(districtRepository.findByNameContainingIgnoreCase(p.getDistrictName()));
		System.out.println(locationInstance.getDistrict().getId());
		stationInstance.setLocation(locationInstance);
		return stationInstance;
	};

	@Override
	public List<Station> getAllStations() {

		return stationRepository.findAll();
	}

	@Override
	public Station editStation(ExternalStation station, String id) {
		Optional<Station> stationExits = stationRepository.findByStationCode(id);
		if (stationExits.isPresent()) {
			Station stationInfoinstance = stationExits.get();
			stationInfoinstance.setCreationDate(station.getCreationDate());
			/* stationInfoinstance.setStationCode(station.getStationCode()); */
			stationInfoinstance.setStationImei(station.getStationImei());
			stationInfoinstance.setStationName(station.getStationName());
			stationInfoinstance.setStationType(stationTypeRepository.findByNameContainingIgnoreCase(station.getStationType()));
			Location locationInstance=stationInfoinstance.getLocation();
			locationInstance.setLatitude(station.getLatitude());
			locationInstance.setLongitude(station.getLongitude());
			locationInstance.setBlock(blockRepository.findByNameContainingIgnoreCase(station.getBlockName()));
			locationInstance.setPanchayat(panchayatRepository.findByNameContainingIgnoreCase(station.getPanchayatName()));
			locationInstance.setDistrict(districtRepository.findByNameContainingIgnoreCase(station.getDistrictName()));
			stationInfoinstance.setLocation(locationInstance);
			return stationRepository.save(stationInfoinstance);
		} else {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No Station availabe in this ID");
		}

	}

	@Override
	public Boolean deleteStation(String id) {
		Boolean flag=false;
		Optional<Station> stationExits = stationRepository.findByStationCode(id);
		if (stationExits.isPresent()) {
			Station stationInfoInstance = stationExits.get();
			stationInfoInstance.setStationType(null);
			Optional<Location> locationExits = locationrepository.findById(stationInfoInstance.getLocation().getId());
			Location locationInstance = null;
			if (locationExits.isPresent()) {
				locationInstance = locationExits.get();
				locationInstance.setBlock(null);
				locationInstance.setDistrict(null);
				locationInstance.setPanchayat(null);

			}
			stationInfoInstance.setLocation(null);
			stationRepository.delete(stationInfoInstance);
			locationrepository.delete(locationInstance);
			flag=true;
			return flag;

		} else {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No Station availabe in this ID");
		}
	}

	@Override
	public Station getStation(String id) {

		Optional<Station> stationExits = stationRepository.findByStationCode(id);
		if (stationExits.isPresent()) {
			return stationExits.get();
		} else {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No Station availabe in this ID");
		}

	}

}

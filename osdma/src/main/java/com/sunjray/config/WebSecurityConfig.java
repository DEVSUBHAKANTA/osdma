
package com.sunjray.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;

import com.sunjray.service.daoImpl.JwtuserdetailsService;
import com.sunjray.util.CustomAuthenticationSuccessHandler;

import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

@Configuration

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private JwtuserdetailsService JwtUserDetailsService;

	@Autowired
	private CustomAuthenticationSuccessHandler sucessHandeller;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(JwtUserDetailsService).passwordEncoder(passwordEncoderBean());
	}

	private PasswordEncoder getPasswordEncoder() {
		return new PasswordEncoder() {

			@Override
			public String encode(CharSequence charSequence) {
				return charSequence.toString();
			}

			@Override
			public boolean matches(CharSequence charSequence, String s) {
				return true;
			}
		};
	}

	@Bean
	@Autowired(required = true)
	public PasswordEncoder passwordEncoderBean() {
		return new BCryptPasswordEncoder();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.csrf().disable().authorizeRequests().antMatchers("/", "/login", "/logout"/* ,"/admin/nav","/admin/**" */)
				.permitAll().antMatchers("/admin/**").hasRole("ADMIN").antMatchers("/hq/**").hasRole("HQ")
				.antMatchers("/user/**").hasRole("USER")
				/* .antMatchers("/admin/**").hasAnyRole("ADMIN") */
				/* .antMatchers("/admin/**").access("hasRole('ROLE_ADMIN')") */
				/* .antMatchers("/admin/**").permitAll() */
				.anyRequest().authenticated().and().formLogin().loginProcessingUrl("/signup").usernameParameter("user")
				.passwordParameter("password").loginPage("/login").successHandler(sucessHandeller).and().logout()
				.invalidateHttpSession(true).clearAuthentication(true)
				.logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/logout-success")
				.permitAll();

	}

	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/resources/**").antMatchers("/assets/**").antMatchers("/coustomjs/**")
				.antMatchers("/assets/image/**");
	}

}

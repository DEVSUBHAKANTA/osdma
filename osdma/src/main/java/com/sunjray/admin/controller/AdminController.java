package com.sunjray.admin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller 
 @RequestMapping("/admin") 
public class AdminController {

	@RequestMapping(value= "/add_new_station",method=RequestMethod.GET)
	public ModelAndView addNewStation(Model model){
		System.out.println("ADD station");
		
		return new ModelAndView("/adminUser/add_Station");	
	}
	
	@RequestMapping(value= "/update_station",method=RequestMethod.GET)
	public ModelAndView addHeadQuarterUser(Model model){
		System.out.println("update station");
		
		return new ModelAndView("/adminUser/UpdateStation");	
	}
	
	@RequestMapping("/nav")
	public String success() {
		return "adminUser/AdminHome";
	}

	@RequestMapping("/realtime")
	public String rtdata() {
		return "adminUser/realtimeDataforAdmin";
	}

	@RequestMapping("/Historical")
	public String histiricalData() {
		return "adminUser/HistoricalDataForAdmin";
	}

	@RequestMapping("/station")
	public String station() {
		return "adminUser/stationdetails";
	}

	@RequestMapping("/createStation")
	public String createStation() {
		return "adminUser/add_Station";
	}

	@RequestMapping("/Profile")
	public String viewProfile() {
		return "adminUser/adminProfileView";
	}
	@RequestMapping("/editProfile")
	public String editProfile() {
		return "common/EditProfile";
	}

	@RequestMapping("/blocknav")
	public String success1() {
		return "blockUser/blockUserHome";
	}
	@RequestMapping("/hqnav")
	public String success2() {
		return "hquser/HQHome";
	}
	
}

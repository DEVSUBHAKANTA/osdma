
package com.sunjray.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.sunjray.json.ExternalUser;
import com.sunjray.model.User;
import com.sunjray.service.dao.UserService;
import com.sunjray.service.daoImpl.JwtuserdetailsService;

@Controller
@RequestMapping("/admin")
public class UserControllerOpt {

	@Autowired
	private JwtuserdetailsService userDetailsService;

	@RequestMapping(value = "/add_new_user", method = RequestMethod.GET)
	public ModelAndView addBlockUser(Model model) {
		System.out.println("ADD BLOCK/HQ USER");

		return new ModelAndView("/adminUser/add_User");
	}

	@RequestMapping(value = "/update_user", method = RequestMethod.GET)
	public String updateEmployee() {
		System.out.println("....UPDATE EMPLOYEE....");
		return "/adminUser/UpdateUser";
	}

	@RequestMapping(value = "/createUser_Ajax", method = RequestMethod.POST)
	public ResponseEntity<User> saveUser(@RequestBody ExternalUser externaluser) {
		System.out.println(externaluser.getUsername());
		User userInstance = ((JwtuserdetailsService) userDetailsService).saveUser(externaluser);
		return new ResponseEntity<User>(userInstance, HttpStatus.OK);
	}

}

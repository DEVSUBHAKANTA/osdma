
package com.sunjray.admin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CommonController {


	@RequestMapping("/login")
	public String Login() {
		return "common/UserLogin";
	}
	
	@RequestMapping("/logout-success")
	public String logoutPage()
	{
		return "common/logout";
	}
}

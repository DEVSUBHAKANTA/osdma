package com.sunjray.admin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.sunjray.model.Station;
import com.sunjray.json.ExternalStation;
import com.sunjray.service.dao.StationService;

@Controller
@RequestMapping("/admin")
public class StationControllerOpt {
	@Autowired
	private StationService stationService;

	@RequestMapping(value = "/createStation", method = RequestMethod.GET)
	public String Station() {
		System.out.println("inside controller createStation()");
		return "adminUser/add_Station";
	}

	@RequestMapping(value = "/createStation_Ajax", method = RequestMethod.POST)
	public ResponseEntity<Station> saveStation(@RequestBody ExternalStation externalStationinfo) {
		com.sunjray.model.Station stationInstance = stationService.saveStation(externalStationinfo);
		return new ResponseEntity<Station>(stationInstance, HttpStatus.OK);
	}

	@RequestMapping(value = "/getStationById_Ajax", method = RequestMethod.GET)
	public ResponseEntity<Station> getStation(@RequestParam("id") String id) {
		Station stationInstance = stationService.getStation(id);
		return new ResponseEntity<Station>(stationInstance, HttpStatus.OK);
	}

	@RequestMapping(value = "/updateStation_Ajax", method = RequestMethod.PUT)
	public ResponseEntity<Station> updateStation(@RequestParam("id") String id,@RequestBody ExternalStation externalStationinfo) throws Exception {
		
		Station stationInstance = stationService.editStation(externalStationinfo, id);
		return new ResponseEntity<Station>(stationInstance, HttpStatus.OK);

	}

	@RequestMapping(value = "/deleteStation_Ajax", method = RequestMethod.DELETE)
	public ResponseEntity<Boolean> deleteStation(@RequestParam("id") String id) {
		Boolean flag=stationService.deleteStation(id);
		return new ResponseEntity<Boolean>(flag,HttpStatus.OK);
	}

	@RequestMapping(value = "/getAllStations", method = RequestMethod.GET)
	public ResponseEntity<List<Station>> getAllStation() {
		List<Station> stations = stationService.getAllStations();
		return new ResponseEntity<List<Station>>(stations, HttpStatus.OK);
	}
}

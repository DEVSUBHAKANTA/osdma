
package com.sunjray.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "USER_INFO")
public class User implements Serializable {

	@Id
	@Column(name = "id")
	@NotNull
	private String username;

	@Column(name = "PASSWORD", length = 100)
	@NotNull
	private String password;

	@Column(name = "FIRSTNAME", length = 50)
	@NotNull
	private String firstname;

	@Column(name = "LASTNAME", length = 50)
	@NotNull
	private String lastname;

	@Column(name = "MIDDLENAME", length = 50)
	private String middlename;
	@Column(name = "EMAIL")
	private String email;
	@Column(name = "PHONE_NUMBER")
	private Long phoneNumber;

	@Column(name = "ENABLED", length = 1)
	@NotNull
	private Integer enabled;

	@JsonIgnore
	@Column(name = "LASTPASSWORDRESETDATE")
	@Temporal(TemporalType.TIMESTAMP)
	@NotNull
	private Date lastPasswordResetDate;

	
	  @OneToOne(cascade = CascadeType.ALL) 
	  @JoinColumn(name = "LOCATION_ID")
	  private Location location;
	 

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "USER_ROLE", joinColumns = {
			@JoinColumn(name = "USER_ID", referencedColumnName = "ID") }, inverseJoinColumns = {
					@JoinColumn(name = "ROLE_ID", referencedColumnName = "ID") })
	 private Set<Role> roles;

    public User() {
    }

    public User(User user) {
       
        this.username = user.getUsername();
        this.firstname=user.getFirstname();
        this.email=user.getEmail();
        this.enabled=user.getEnabled();
        this.lastname=user.getLastname();
        this.middlename=user.getMiddlename();
        this.lastPasswordResetDate=user.getLastPasswordResetDate();
        this.location=user.getLocation();
        this.phoneNumber=user.getPhoneNumber();
     
        this.password = user.getPassword();
        this.roles = user.getRoles();
    }

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getMiddlename() {
		return middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(Long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

	public Date getLastPasswordResetDate() {
		return lastPasswordResetDate;
	}

	public void setLastPasswordResetDate(Date lastPasswordResetDate) {
		this.lastPasswordResetDate = lastPasswordResetDate;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	

}

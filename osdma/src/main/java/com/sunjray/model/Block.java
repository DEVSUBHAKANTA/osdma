
package com.sunjray.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
@Table(name = "BLOCK")
public class Block {
	@Id
	private Integer id;
	private String name;
	private String code;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	@OneToMany(mappedBy="block")
	@JsonIgnore
	private List<Panchayat> panchayats;
	
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "DISTRICT_ID")
	private District district;
	public List<Panchayat> getPanchayats() {
		return panchayats;
	}
	public void setPanchayats(List<Panchayat> panchayats) {
		this.panchayats = panchayats;
	}
	public District getDistrict() {
		return district;
	}
	public void setDistrict(District district) {
		this.district = district;
	}
	
	
}

/*
 * package com.sunjray.model;
 * 
 * import java.io.Serializable; import java.util.UUID;
 * 
 * import javax.persistence.CascadeType; import javax.persistence.Column; import
 * javax.persistence.Entity; import javax.persistence.Id; import
 * javax.persistence.JoinColumn; import javax.persistence.OneToOne; import
 * javax.persistence.Table;
 * 
 * @Entity
 * 
 * @Table(name = "ADRESS") public class Adress implements Serializable {
 * 
 * @Id private String id; private String state; private String country;
 * 
 * public String getId() { return id; }
 * 
 * public void setId(String id) { this.id = id; }
 * 
 * public String getState() { return state; }
 * 
 * public void setState(String state) { this.state = state; }
 * 
 * public String getCountry() { return country; }
 * 
 * public void setCountry(String country) { this.country = country; }
 * 
 * @OneToOne(cascade = CascadeType.ALL)
 * 
 * @JoinColumn(name = "DISTRICT_ID") private District district;
 * 
 * @OneToOne(cascade = CascadeType.ALL)
 * 
 * @JoinColumn(name = "BLOCK_ID") private Block block;
 * 
 * @OneToOne(cascade = CascadeType.ALL)
 * 
 * @JoinColumn(name = "PANCHAYAT_ID") private Panchayat panchayat;
 * 
 * public District getDistrict() { return district; }
 * 
 * public void setDistrict(District district) { this.district = district; }
 * 
 * public Block getBlock() { return block; }
 * 
 * public void setBlock(Block block) { this.block = block; }
 * 
 * public Panchayat getPanchayat() { return panchayat; }
 * 
 * public void setPanchayat(Panchayat panchayat) { this.panchayat = panchayat; }
 * 
 * }
 */
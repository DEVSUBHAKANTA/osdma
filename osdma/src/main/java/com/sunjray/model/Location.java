
package com.sunjray.model;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "LOCATION")
public class Location implements Serializable {

	@Id
	@Column(name = "id")
	private String id = UUID.randomUUID().toString().toUpperCase();

	@Column(name = "LATITUDE")
	private String latitude;

	@Column(name = "LONGITUDE")
	private String longitude;

	public String getId() {
		return id;
	}

	public String getLatitude() {
		return latitude;
	}

	public String getLongitude() {
		return longitude;
	}
	

	public void setId(String id) {
		this.id = id;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "DISTRICT_ID")
	private District district;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "BLOCK_ID")
	private Block block;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "PANCHAYAT_ID")
	private Panchayat panchayat;

	public District getDistrict() {
		return district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

	public Block getBlock() {
		return block;
	}

	public void setBlock(Block block) {
		this.block = block;
	}

	public Panchayat getPanchayat() {
		return panchayat;
	}

	public void setPanchayat(Panchayat panchayat) {
		this.panchayat = panchayat;
	}
}

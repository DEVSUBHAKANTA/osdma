
package com.sunjray.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity

@Table
public class ParameterUnit {

	@Id

	@Column(name = "id")
	private int id;
	private String parameterName;
	private String parameterUnit;

	public int getId() {
		return id;
	}

	public String getParameterName() {
		return parameterName;
	}

	public String getParameterUnit() {
		return parameterUnit;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}

	public void setParameterUnit(String parameterUnit) {
		this.parameterUnit = parameterUnit;
	}

}

package com.sunjray;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


/*
 * @EntityScan("com.sunjray.*")
 * 
 * @EnableJpaRepositories("com.sunjray.*")
 * 
 * @ComponentScan(basePackages = { "com.sunjray.*" })
 */
@SpringBootApplication
public class OsdmaApplication {

	public static void main(String[] args) {
		SpringApplication.run(OsdmaApplication.class, args);
	}

}


package com.sunjray.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sunjray.model.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {
	Role findByNameContainingIgnoreCase(String name);
}


package com.sunjray.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sunjray.model.Station;

@Repository
public interface StationRepository extends JpaRepository<Station, String> {
		Optional<Station> findByStationCode(String id);

}

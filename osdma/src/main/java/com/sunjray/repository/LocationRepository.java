
package com.sunjray.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sunjray.model.Location;

@Repository
public interface LocationRepository extends JpaRepository<Location, String> {

}


package com.sunjray.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sunjray.model.Block;
import com.sunjray.model.District;

@Repository
public interface DistrictRepository extends JpaRepository<District, Integer> {
	District findByNameContainingIgnoreCase(String name);

}

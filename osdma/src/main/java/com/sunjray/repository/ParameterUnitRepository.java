
package com.sunjray.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sunjray.model.ParameterUnit;
@Repository
public interface ParameterUnitRepository extends JpaRepository<ParameterUnit, Integer> {

}

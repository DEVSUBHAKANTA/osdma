package com.sunjray.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sunjray.model.Block;
import com.sunjray.model.Panchayat;

@Repository
public interface BlockRepository extends JpaRepository<Block, Integer> {
	Block findByNameContainingIgnoreCase(String name);
}


package com.sunjray.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.sunjray.model.StationType;

@Repository
public interface StationTypeRepository extends JpaRepository<StationType, Integer> {

				StationType findByNameContainingIgnoreCase(String name);

}

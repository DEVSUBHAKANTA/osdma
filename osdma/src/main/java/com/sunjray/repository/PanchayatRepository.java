package com.sunjray.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.sunjray.model.Panchayat;
@Repository
public interface PanchayatRepository extends JpaRepository<Panchayat, Integer> {

	Panchayat findByNameContainingIgnoreCase(String name);

}

package com.sunjray.json;

import java.util.List;

import javax.validation.constraints.NotNull;

public class ExternalUser {
	
	@NotNull
	private String username;
	@NotNull
	private String password;
	@NotNull
	private String firstname;
	private String lastname;
	@NotNull
	private String middlename;
	private String email;	
	private Long phoneNumber;
	@NotNull
	/* private List<String> roles; */
	private String roles;
	@NotNull
	private String districtName;
	private String districtCode;
	@NotNull
	private String blockName;
	private String blockCode;
	@NotNull
	private String panchayatName;
	private String panchayatCode;
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getMiddlename() {
		return middlename;
	}
	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Long getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(Long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getDistrictName() {
		return districtName;
	}
	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}
	public String getDistrictCode() {
		return districtCode;
	}
	public void setDistrictCode(String districtCode) {
		this.districtCode = districtCode;
	}
	public String getBlockName() {
		return blockName;
	}
	public void setBlockName(String blockName) {
		this.blockName = blockName;
	}
	public String getBlockCode() {
		return blockCode;
	}
	public void setBlockCode(String blockCode) {
		this.blockCode = blockCode;
	}
	public String getPanchayatName() {
		return panchayatName;
	}
	public void setPanchayatName(String panchayatName) {
		this.panchayatName = panchayatName;
	}
	public String getPanchayatCode() {
		return panchayatCode;
	}
	public void setPanchayatCode(String panchayatCode) {
		this.panchayatCode = panchayatCode;
	}

	/*
	 * public List<String> getRoles() { return roles; } public void
	 * setRoles(List<String> roles) { this.roles = roles; }
	 */
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public void setRoles(String roles) {
		this.roles = roles;
	}
	public String getRoles() {
		return roles;
	}
	
}
